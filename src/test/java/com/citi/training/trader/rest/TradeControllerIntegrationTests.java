package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.Trade;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.TradeController}.
 *
 */
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"h2", "no-scheduled"})
public class TradeControllerIntegrationTests {

    private static final Logger logger =
                        LoggerFactory.getLogger(TradeControllerIntegrationTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    private static final String tradeBasePath = "/api/v1/trades";

    @Test
    public void findAll_returnsList() {

        ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
                                tradeBasePath,
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Trade>>(){});

        assertEquals(HttpStatus.OK, findAllResponse.getStatusCode());

        for(Trade trade: findAllResponse.getBody()) {
            logger.info("Found Trade: " + trade);
        }

        assertTrue("trade list should be greater than zero as src/test/resources/data.sql should be loaded",
                   findAllResponse.getBody().size() > 0);

        
        ResponseEntity<Trade> findByIdResponse = restTemplate.exchange(
                tradeBasePath + "/" + findAllResponse.getBody().get(0).getId(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Trade>(){});

        assertEquals("FindById should return 200 OK",
                     HttpStatus.OK, findByIdResponse.getStatusCode());

        ResponseEntity<Trade> notFoundResponse = restTemplate.exchange(
                tradeBasePath + "/4999",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Trade>(){});

        assertEquals("FindById for invalid id should return 404",
                     HttpStatus.NOT_FOUND, notFoundResponse.getStatusCode());
    }
}
