package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class MovingAverageStrategyTests {
	
	private int testStockId = 12;
	private String testStockTicker = "GOOG";
	private int testId = 1;
	private Stock testStock = new Stock(testStockId, testStockTicker);
    private int testSize = 10;
    private double testPercentage = 0.73;
    private int testLongPeriod = 400;
    private int testShortPeriod = 20;
    private double testExitProfitLoss = 0.65;
    private int testCurrentPosition = 17;
    private double testLastTradePrice = 1.53;
    private double testProfit = -0.12;
    private boolean testAbove = true;
    private Date testStopped = new Date();
    private double testPrevProfit = 1.98;
    private boolean testStart = true;
    
    @Test
    public void test_MovAvStrat_defaultConstructorAndSetters() {
		MovingAverageStrategy testStrat = new MovingAverageStrategy();
		testStrat.setId(testId);
		testStrat.setStock(testStock);
		testStrat.setSize(testSize);
		testStrat.setPercentage(testPercentage);
		testStrat.setLongPeriod(testLongPeriod);
        testStrat.setShortPeriod(testShortPeriod);
        testStrat.setExitProfitLoss(testExitProfitLoss);
        testStrat.setCurrentPosition(testCurrentPosition);
        testStrat.setLastTradePrice(testLastTradePrice);
        testStrat.setProfit(testProfit);
        testStrat.setAbove(testAbove);
        testStrat.setStopped(testStopped);
        testStrat.setPrevProfit(testPrevProfit);
        testStrat.setStart(testStart);
        
        // Constructor methods
        assertEquals("Strategy Id should be equal", testId, testStrat.getId());
        assertEquals("Stock ticker should be equal", testStockTicker, testStrat.getStock().getTicker());
        assertEquals("Strat size should be equal", testSize, testStrat.getSize());
        assertEquals("Strat percentage should be equal", testPercentage, testStrat.getPercentage(), 0.00001);
        assertEquals("Strat long period should be equal", testLongPeriod, testStrat.getLongPeriod());
        assertEquals("Strat short period should be equal", testShortPeriod, testStrat.getShortPeriod());
        assertEquals("Strat exit profit loss should be equal", testExitProfitLoss, testStrat.getExitProfitLoss(), 0.00001);
        assertEquals("Strat current position should be equal", testCurrentPosition, testStrat.getCurrentPosition());
        assertEquals("Strat last trade price should be equal", testLastTradePrice, testStrat.getLastTradePrice(), 0.00001);
        assertEquals("Strat profit should be equal", testProfit, testStrat.getProfit(), 0.00001);
        assertEquals("Strat above should be equal", testAbove, testStrat.isAbove());
        assertEquals("Strat stopped date should be equal", testStopped, testStrat.getStopped());
        assertEquals("Strat prev profit should be equal", testPrevProfit, testStrat.getPrevProfit(), 0.00001);
        assertEquals("Strat start should be equal", testStart, testStrat.isStart());
    }
    
    @Test
    public void test_MovAvStrat_simple_funcs() {
    	
    	MovingAverageStrategy testStrat2 = new MovingAverageStrategy();
		testStrat2.setId(testId);
		testStrat2.setStock(testStock);
		testStrat2.setSize(testSize);
		testStrat2.setPercentage(testPercentage);
		testStrat2.setLongPeriod(testLongPeriod);
        testStrat2.setShortPeriod(testShortPeriod);
        testStrat2.setExitProfitLoss(testExitProfitLoss);
        testStrat2.setCurrentPosition(testCurrentPosition);
        testStrat2.setLastTradePrice(testLastTradePrice);
        testStrat2.setProfit(testProfit);
        testStrat2.setAbove(testAbove);
        testStrat2.setStopped(testStopped);
        testStrat2.setPrevProfit(testPrevProfit);
        testStrat2.setStart(testStart);
        
        assertTrue("testStrat should have Long Position", testStrat2.hasLongPosition());
        assertTrue("testStrat should have a position", testStrat2.hasPosition());
        //assertTrue("testStrat should have a short position", testStrat2.hasShortPosition());
        
    	
    }
}
