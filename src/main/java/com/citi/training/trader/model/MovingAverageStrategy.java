package com.citi.training.trader.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * A model object to hold the data for an instance of the MovingAverage algorithm.
 *
 * This stratagy will take a long and short average. It will compare the two averages. 
 * 
 * It is used to measure the trend of a stock. A crossover occurs when a faster moving average 
 * (shorter period moving average) crosses over a slower moving average or below.
 * 
 * See {@link com.citi.training.trader.strategy.MoveingAverageStrategy}.
 *
 */
@Entity
public class MovingAverageStrategy {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Stock stock;
    private int size;
    private double percentage;
    private double exitProfitLoss;
    private int currentPosition;
    private double lastTradePrice;
    private double profit;
    private int longPeriod;
    private int shortPeriod;
    private boolean above;
    private Date stopped;
    private double prevProfit;
    private boolean start;

    public MovingAverageStrategy() {}

    public MovingAverageStrategy(int id, Stock stock, int size, double percentage,
    					  int longPeriod, int shortPeriod,
                          double exitProfitLoss, int currentPosition,
                          double lastTradePrice, double profit, boolean above,
                          Date stopped, boolean start) {
        this.id = id;
        this.stock = stock;
        this.setLongPeriod(longPeriod);
        this.setShortPeriod(shortPeriod);
        this.setSize(size);
        this.setPercentage(percentage);
        this.setExitProfitLoss(exitProfitLoss);
        this.setCurrentPosition(currentPosition);
        this.setLastTradePrice(lastTradePrice);
        this.setProfit(profit);
        this.setStopped(stopped);
        this.setAbove(above);
        this.setStart(start);
    }
    
    /*
     * 
     * Setters and getters
     * 
     */
    
    public int getId() {
    	return id;
    }
    
    public void setId(int id) {
    	this.id = id;
    }
    
    public Stock getStock() {
    	return stock;
    }
    
    public void setStock(Stock stock) {
    	this.stock = stock;
    }
    

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public double getExitProfitLoss() {
		return exitProfitLoss;
	}

	public void setExitProfitLoss(double exitProfitLoss) {
		this.exitProfitLoss = exitProfitLoss;
	}

	public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}

	public double getLastTradePrice() {
		return lastTradePrice;
	}

	public void setLastTradePrice(double lastTradePrice) {
		this.lastTradePrice = lastTradePrice;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public int getLongPeriod() {
		return longPeriod;
	}

	public void setLongPeriod(int longPeriod) {
		this.longPeriod = longPeriod;
	}

	public int getShortPeriod() {
		return shortPeriod;
	}

	public void setShortPeriod(int shortPeriod) {
		this.shortPeriod = shortPeriod;
	}

	public Date getStopped() {
		return stopped;
	}

	public void setStopped(Date stopped) {
		this.stopped = stopped;
	}
	
    public void addProfitLoss(double profitLoss) {
        this.profit += profitLoss;
        if(Math.abs(this.profit) >= exitProfitLoss) {
            this.setStopped(new Date());
        }
    }
    
    public void stop() {
        this.stopped = new Date();
    }

    public boolean hasPosition() {
        return this.currentPosition != 0;
    }

    public boolean hasShortPosition() {
        return this.currentPosition < 0;
    }

    public boolean hasLongPosition() {
        return this.currentPosition > 0;
    }

    public void takeShortPosition() {
        this.currentPosition = -1;
    }

    public void takeLongPosition() {
        this.currentPosition = 1;
    }
    
    public void closePosition() {
        this.currentPosition = 0;
    }
    
    public boolean isAbove() {
		return above;
	}

	public void setAbove(boolean above) {
		this.above = above;
	}

	public double getPrevProfit() {
		return prevProfit;
	}

	public void setPrevProfit(double prevProfit) {
		this.prevProfit = prevProfit;
	}
	
    @Override
    public String toString() {
        return "Strategy [id=" + id + ", stock=" + stock +
                ", size=" + size + ", maxTrades=" + exitProfitLoss +
                ", currentPosition=" + currentPosition +
                ", stopped=" + stopped + "]";
    }

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}

	

}
