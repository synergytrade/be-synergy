package com.citi.training.trader.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Modal class for Stock, defining the Table structure in DB and defining simple setters and getters
 *
 */
@Entity
public class Stock {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String ticker;

    public Stock() {}

    public Stock(int id, String ticker) {
        this.id = id;
        this.ticker = ticker;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    @Override
    public String toString() {
        return "Stock [id=" + id + ", ticker=" + ticker + "]";
    }
}
