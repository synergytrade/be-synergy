package com.citi.training.trader.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.Stock;
import com.google.common.collect.Lists;

@Component
public class StockService {

    private static final Logger LOG =
                            LoggerFactory.getLogger(StockService.class);

    @Autowired
    private StockDao stockDao;

    public Stock save(Stock stock) {
        return stockDao.save(stock);
    }

    public List<Stock> findAll() {
        List<Stock> stocks = Lists.newArrayList(stockDao.findAll());
        LOG.info("Found stocks: " + stocks);
        return stocks;
    }

    public Stock findById(int id) {
        try {
            return stockDao.findById(id).get();
        } catch(NoSuchElementException ex) {
            String msg = "Stock not found for id: [" + id + "]";
            LOG.warn(msg);
            throw new EntityNotFoundException(msg);
        }
    }

    public Stock findByTicker(String ticker) {
        return stockDao.findByTicker(ticker);
    }

    public void deleteById(int id) {
        try {
            stockDao.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            String msg = "Delete failed, no stock with id [" + id + "]";
            LOG.warn(msg);
            throw new EntityNotFoundException(msg);
        }
    }
}
