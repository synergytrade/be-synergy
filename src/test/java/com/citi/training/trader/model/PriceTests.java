package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
@Ignore
public class PriceTests {

    private Stock testStock = new Stock(2, "AAPL");
    private double testPrice = 200.99;
    private Date testRecordedAt = new Date();

    @Test
    public void test_Price_fullConstructor() {
        Price testPriceObj = new Price();
        testPriceObj.setStock(testStock);
        testPriceObj.setPrice(testPrice);
        testPriceObj.setRecordedAt(testRecordedAt);

        assertEquals("The test object should contain the stock ticker given in constructor",
                    testStock.getTicker(), testPriceObj.getStock().getTicker());

        assertEquals("The test object should contain the price given in constructor",
                    testPrice, testPriceObj.getPrice(), 0.000001);

        assertEquals("The test object should contain the recordedAt Date given in constructor",
                    testRecordedAt, testPriceObj.getRecordedAt());
    }

    @Test
    public void test_Price_toString() {
        Price testPriceObj = new Price(testStock, testPrice, testRecordedAt);

        assertTrue("Price.toString should contain the stock ticker given in constructor",
                   testPriceObj.toString().contains(testStock.getTicker()));

        assertTrue("Price.toString() should contain the price given in constructor",
                   testPriceObj.toString().contains(Double.toString(testPrice)));
    }
}
