package com.citi.training.trader.rest;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.citi.training.trader.model.Graph;
import com.citi.training.trader.model.MovingAverageStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.GraphService;
import com.citi.training.trader.service.MovingAverageStrategyService;
import com.citi.training.trader.service.TradeService;

/**
 * REST Controller for {@link com.citi.training.trader.model.Trade} resource.
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/graphs")

public class GraphController {
	
	private static final Logger LOG =
            LoggerFactory.getLogger(TradeController.class);

	@Autowired
	private GraphService graphService;
	
	
	
	@RequestMapping(method=RequestMethod.GET,
	            produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Graph> findAll() {
	LOG.info("findAll()");
	return graphService.findAll();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET,
	            produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Graph> findAllByStrat(@PathVariable int id) {
	LOG.info("findById [" + id + "]");
	
	return graphService.findAllByStrat(id);
	}
}