package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.StockService;

/**
 * REST Controller for {@link com.citi.training.trader.model.Stock} resource.
 *
 */
//@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host:*}")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/stocks")
public class StockController {

    private static final Logger LOG =
                    LoggerFactory.getLogger(StockController.class);

    @Autowired
    private StockService stockService;

    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Stock> findAll() {
        LOG.info("findAll()");
        return stockService.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Stock findById(@PathVariable int id) {
        LOG.info("findById [" + id + "]");
        return stockService.findById(id);
    }

    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Stock> create(@RequestBody Stock stock) {
        LOG.info("HTTP POST : create [" + stock + "]");

        Stock createdStock = stockService.save(stock);
        LOG.info("created stock: [" + createdStock + "]");

        return new ResponseEntity<Stock>(createdStock, HttpStatus.CREATED);
    }
    

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        LOG.info("deleteById [" + id + "]");
        stockService.deleteById(id);
    }
}
