package com.citi.training.trader.rest;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;

import com.citi.training.trader.model.MovingAverageStrategy;
import com.citi.training.trader.service.MovingAverageStrategyService;
import org.springframework.http.MediaType;


/**
 * REST Controller for {@link com.citi.training.trader.model.MovingAverageController} resource.
 * 
 * <p> This is creating end points to access Moving Stratagy  </p>
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/movingaverage")
public class MovingAverageController {
	
    private static final Logger LOG = LoggerFactory.getLogger(MovingAverageController.class);

    @Autowired
    private MovingAverageStrategyService movingAverageStrategyService;
    
    
    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MovingAverageStrategy> findAll() {
    	LOG.info("findAll()");
    	return movingAverageStrategyService.findAll();
    }
    
    @RequestMapping(value = "/stop/{id}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void stopMovingAvarege(@PathVariable Integer id){
    	MovingAverageStrategy savedOne = movingAverageStrategyService.getById(id);
    	Date date = new Date();
    	savedOne.setStopped(date);
    	movingAverageStrategyService.save(savedOne);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        LOG.info("deleteById [" + id + "]");
        movingAverageStrategyService.deleteById(id);
    }
    
    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<MovingAverageStrategy> create(@RequestBody MovingAverageStrategy strat){
    	 LOG.info("HTTP POST : create[" + strat + "]");

         MovingAverageStrategy createdSimpleStrategy = movingAverageStrategyService.save(strat);
         LOG.info("created simpleStrategy: [" + createdSimpleStrategy + "]");

         return new ResponseEntity<MovingAverageStrategy>(createdSimpleStrategy, HttpStatus.CREATED);
     }
    
    @RequestMapping(value = "/current" ,method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MovingAverageStrategy> findAllCurrent() {
    	LOG.info("findAll()Current");
    	return movingAverageStrategyService.findAllCurrent();
    }
    
  
    
}



