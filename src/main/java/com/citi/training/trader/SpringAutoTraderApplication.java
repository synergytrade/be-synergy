package com.citi.training.trader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Starts up Spring boot aplication
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.StockController}.
 *
 */

@SpringBootApplication
@EnableScheduling
public class SpringAutoTraderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAutoTraderApplication.class, args);
	}

}
