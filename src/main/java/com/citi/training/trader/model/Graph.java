package com.citi.training.trader.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Modal class defining the Table structure in DB and defining simple setters and getters
 *
 */
@Entity
public class Graph {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	private MovingAverageStrategy strat;
	
	private double avgShort;
	private double avgLong;
	private LocalDateTime createdTime = LocalDateTime.now();
	
	public Graph() {}

	public Graph(int id, MovingAverageStrategy strat, double avgShort, double avgLong, LocalDateTime currentTime) {
		this.id = id;
		this.strat = strat;
		this.avgShort = avgShort;
		this.avgLong = avgLong;
		this.createdTime = currentTime;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the stratId
	 */
	public MovingAverageStrategy getStratId() {
		return strat;
	}

	/**
	 * @param stratId the stratId to set
	 */
	public void setStratId(MovingAverageStrategy strat) {
		this.strat = strat;
	}

	/**
	 * @return the avgShort
	 */
	public double getAvgShort() {
		return avgShort;
	}

	/**
	 * @param avgShort the avgShort to set
	 */
	public void setAvgShort(double avgShort) {
		this.avgShort = avgShort;
	}

	/**
	 * @return the avgLong
	 */
	public double getAvgLong() {
		return avgLong;
	}

	/**
	 * @param avgLong the avgLong to set
	 */
	public void setAvgLong(double avgLong) {
		this.avgLong = avgLong;
	}

	/**
	 * @return the currentTime
	 */
	public LocalDateTime getCurrentTime() {
		return createdTime;
	}

	/**
	 * @param currentTime the currentTime to set
	 */
	public void setCurrentTime(LocalDateTime currentTime) {
		this.createdTime = currentTime;
	}

	@Override
	public String toString() {
		return "Graph [id=" + id + ", stratId=" + strat + ", avgShort=" + avgShort + ", avgLong=" + avgLong
				+ ", currentTime=" + createdTime + "]";
	}
	
	
	

}
