package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.Date;
import org.junit.Test;

public class GraphTests {
	
	
	// Variables for testStock
	private Stock testStock = new Stock(12, "GOOG");
	
	// Variables for testStrat
	private int testIdStrat = 1;
    private int testSize = 10;
    private double testPercentage = 0.73;
    private double testExitProfitLoss = 0.65;
    private int testCurrentPosition = 17;
    private double testLastTradePrice = 1.53;
    private double testProfit = -0.12;
    private int testLongPeriod = 400;
    private int testShortPeriod = 20;
    private boolean testAbove = true;
    private Date testStopped = new Date();
    private boolean testStart = true;
	
	// Variables for testGraph
    
	private int testIdGraph = 1;
	private MovingAverageStrategy testStrat = new MovingAverageStrategy(testIdStrat, testStock, testSize, testPercentage, testLongPeriod,
			testShortPeriod, testExitProfitLoss, testCurrentPosition, testLastTradePrice, testProfit, testAbove, testStopped, testStart);
	private double testAvgShort;
	private double testAvgLong;
	private LocalDateTime testCreatedTime = LocalDateTime.now();
	
	
	@Test
    public void test_Graph_defaultConstructorAndSetters() {
		Graph testGraph = new Graph();
		testGraph.setId(testIdGraph);
		testGraph.setStratId(testStrat);
		testGraph.setAvgShort(testAvgShort);
		testGraph.setAvgLong(testAvgLong);
		testGraph.setCurrentTime(testCreatedTime);
        
		Graph testGraph2 = new Graph(testIdGraph, testStrat, testAvgShort, testAvgLong, testCreatedTime);
        assertEquals("Graph id should be equal to testId", testIdStrat, testGraph2.getId());
        assertEquals("Strategy Id should be equal to testStratId", testIdStrat, testGraph2.getStratId().getId());
        assertEquals("avgShort should be equal to testAvgShort", testAvgShort, testGraph2.getAvgShort(), 0.00001);
        assertEquals("avgLong should be equal to testAvgLong", testAvgLong, testGraph2.getAvgLong(), 0.00001);
        assertEquals("Dates are equal", testCreatedTime, testGraph2.getCurrentTime());
	}
	
	@Test
    public void test_Graph_toString() {
		Graph testGraph2 = new Graph(testIdGraph, testStrat, testAvgShort, testAvgLong, testCreatedTime);
		
        assertTrue("Graph.toString() should contain the correct id number",
                   testGraph2.toString().contains(Integer.toString(testGraph2.getId())));
        
        assertTrue("Graph.toString() should contain the correct short average value",
                testGraph2.toString().contains(Double.toString(testGraph2.getAvgShort())));
        
        assertTrue("Graph.toString() should contain the correct long average value",
                testGraph2.toString().contains(Double.toString(testGraph2.getAvgLong())));
                
        assertTrue("Graph.toString() should contain the correct time",
                        testGraph2.toString().contains(Double.toString(testGraph2.getAvgLong())));
}
}
