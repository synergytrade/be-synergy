package com.citi.training.trader.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.GraphDao;
import com.citi.training.trader.model.Graph;
import com.citi.training.trader.model.MovingAverageStrategy;
import com.google.common.collect.Lists;


@Component
public class GraphService {
	
    private static final Logger LOG =
            LoggerFactory.getLogger(GraphService.class);

    @Autowired
    private GraphDao graphDao;
    
    @Autowired
	private MovingAverageStrategyService movingAverageStrategyService;
    
    public Graph save(Graph graph) {
//    	LOG.debug(marker, msg);
    	// remember to log
        return graphDao.save(graph);
    }
	
    public List<Graph> findAll() {
        return Lists.newArrayList(graphDao.findAll());
    }
    
    public List<Graph> findAllByStrat(int id) {
    	MovingAverageStrategy strat = new MovingAverageStrategy();
    	strat = movingAverageStrategyService.getById(id);
        return graphDao.findByStrat(strat);
    }	
    
}

