package com.citi.training.trader.rest;

import java.util.Date;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;

import com.citi.training.trader.model.MovingAverageStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.MovingAverageStrategyService;
import com.citi.training.trader.service.TradeService;

import org.springframework.http.MediaType;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/Statistics")
public class OverallStatsController {

	private static final Logger LOG = LoggerFactory.getLogger(MovingAverageController.class);

    @Autowired
    private MovingAverageStrategyService movingAverageStrategyService;
    
    @Autowired
    private TradeService tradeService;
    
    @RequestMapping(value = "/ProfLoss",method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public double findPnL() {
    	double profitLoss = 0.0;
    	for (MovingAverageStrategy strategy: movingAverageStrategyService.findAll()) {
    		profitLoss += strategy.getProfit();
    	}
    	return profitLoss;
    }
    
    @RequestMapping(value = "/TradesExicuted", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public int TradeNo() {
    	return tradeService.findAll().size();
    }
}
