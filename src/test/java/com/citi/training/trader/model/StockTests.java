package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;


public class StockTests {

    private int testId = 1999;
    private String testTicker = "TESTT";

    @Test
    public void test_Stock_fullConstructor() {
        Stock testStock = new Stock();
        testStock.setId(testId);
        testStock.setTicker(testTicker);

        assertEquals("Stock Id should equal value given in constructor",
                     testId, testStock.getId());

        assertEquals("Stock ticker should match value given in constructor",
                     testTicker, testStock.getTicker());
    }
}
