package com.citi.training.trader.model;

import java.util.Date;

/**
 * Modal class defining how the price object is structured. This object is used in the feed from
 *
 *the API
 */
public class Price {

    private Stock stock;
    private double price;
    private Date recordedAt;
    
    public Price () {}

    public Price(Stock stock, double price, Date recordedAt) {
        this.stock = stock;
        this.price = price;
        this.recordedAt = recordedAt;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getRecordedAt() {
        return recordedAt;
    }

    public void setRecordedAt(Date recordedAt) {
        this.recordedAt = recordedAt;
    }

    @Override
    public String toString() {
        return "Price [stock=" + stock + ", price=" + price +
               ", recordedAt=" + recordedAt + "]";
    }
}
