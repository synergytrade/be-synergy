package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class TradeTests {

    private int defaultId = -1;
    private double testPrice = 99.99;
    private int testTradeSize = 1500;
    private Trade.TradeType testTradeType = Trade.TradeType.BUY;
    private MovingAverageStrategy testTradeStrat = new MovingAverageStrategy();

    @Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testPrice, testTradeSize,
                                    testTradeType, testTradeStrat);

        assertEquals("Create Trade should contain default id of -1",
                     defaultId, testTrade.getId().intValue());

        assertEquals("Created Trade should contain price given in constructor",
                     testPrice, testTrade.getPrice(), 0.00001);

        assertEquals("Created Trade should contain size given in constructor",
                     testTradeSize, testTrade.getSize());

        assertEquals("Created Trade should contain trade type given in constructor",
                     testTradeType, testTrade.getTradeType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_TradeType_fromXmlString() {
        assertEquals("The XML String \"true\" should be converted to TradeType BUY",
                     Trade.TradeType.BUY, Trade.TradeType.fromXmlString("true"));

        assertEquals("The XML String \"false\" should be converted to TradeType SELL",
                     Trade.TradeType.SELL, Trade.TradeType.fromXmlString("false"));
    
        Trade.TradeType.fromXmlString("TTRUE");
    }
    
	@Test
    public void test_Trade_toString() {
		Trade testTradeforString = new Trade(testPrice, testTradeSize, testTradeType, null);
		
        assertTrue("Trade.toString() should contain the correct price",
                   testTradeforString.toString().contains(Double.toString(testTradeforString.getPrice())));
        
        assertTrue("Trade.toString() should contain the correct trade size",
                testTradeforString.toString().contains(Integer.toString(testTradeforString.getSize())));

	}	
	
	
}
