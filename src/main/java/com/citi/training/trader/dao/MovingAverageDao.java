package com.citi.training.trader.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.MovingAverageStrategy;

/**
 * 
 * Creates a DAO object for the Moving Average Strat method, linking it to the db vis the crud repository
 *
 */

public interface MovingAverageDao extends CrudRepository<MovingAverageStrategy, Integer> {

}
