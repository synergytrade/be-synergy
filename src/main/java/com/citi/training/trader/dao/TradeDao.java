package com.citi.training.trader.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Trade;


/**
 * 
 * Creates a DAO object for the Trade method, linking it to the db vis the crud repository
 *
 */
public interface TradeDao extends CrudRepository<Trade, Integer>{

    List<Trade> findByState(Trade.TradeState state);

    Trade findFirstByStrategyIdOrderByLastStateChangeDesc(int strategyId);

    void deleteById(int id);
}
