package com.citi.training.trader.rest;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.http.MediaType;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/status")
public class StatusController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StatusController.class);
	
	@RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String status() {
    	LOG.info("status check");
    	return "Status OK";
    }
}
