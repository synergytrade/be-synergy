-- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: localhost    Database: traderdb
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS traderdb;

USE traderdb;


--
-- Table structure for table `graph`
--


DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avg_long` double NOT NULL,
  `avg_short` double NOT NULL,
  `created_time` datetime DEFAULT NULL,
  `strat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6hk0179l7rfog9sfudl2kuvao` (`strat_id`),
  CONSTRAINT `FK6hk0179l7rfog9sfudl2kuvao` FOREIGN KEY (`strat_id`) REFERENCES `moving_average_strategy` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=647 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph`
--

LOCK TABLES `graph` WRITE;
/*!40000 ALTER TABLE `graph` DISABLE KEYS */;
/*!40000 ALTER TABLE `graph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moving_average_strategy`
--

DROP TABLE IF EXISTS `moving_average_strategy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moving_average_strategy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_position` int(11) NOT NULL,
  `exit_profit_loss` double NOT NULL,
  `last_trade_price` double NOT NULL,
  `profit` double NOT NULL,
  `size` int(11) NOT NULL,
  `stopped` datetime DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `prev_profit` double DEFAULT NULL,
  `short_period` int(11) NOT NULL,
  `long_period` int(11) NOT NULL,
  `percentage` int(11) DEFAULT NULL,
  `above` tinyint(1) DEFAULT NULL,
  `start` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqxxthvr4vkuw8nr5aeguyen38` (`stock_id`),
  CONSTRAINT `FKqxxthvr4vkuw8nr5aeguyen38` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moving_average_strategy`
--

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double NOT NULL,
  `recorded_at` datetime DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5asiec6kmxfhvd4p5irtnihay` (`stock_id`),
  CONSTRAINT `FK5asiec6kmxfhvd4p5irtnihay` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticker` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=527 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock`
--

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` VALUES (1,'a'),(2,'aa'),(3,'aal'),(4,'aapl'),(5,'abbv'),(6,'abc'),(7,'abt'),(8,'abx'),(9,'acn'),(10,'adbe'),(11,'adi'),(12,'adm'),(13,'adp'),(14,'ads'),(15,'adsk'),(16,'adt'),(17,'aee'),(18,'aep'),(19,'aes'),(20,'aet'),(21,'afl'),(22,'agn'),(23,'aig'),(24,'aiv'),(25,'akam'),(26,'aks'),(27,'all'),(28,'alxn'),(29,'amat'),(30,'amd'),(31,'ame'),(32,'amgn'),(33,'amp'),(34,'amt'),(35,'amzn'),(36,'an'),(37,'anf'),(38,'antm'),(39,'aon'),(40,'apa'),(41,'apc'),(42,'apd'),(43,'aph'),(44,'ati'),(45,'atvi'),(46,'avb'),(47,'avgo'),(48,'avp'),(49,'avy'),(50,'axp'),(51,'ba'),(52,'bac'),(53,'bax'),(54,'bbby'),(55,'bbt'),(56,'bby'),(57,'bc'),(58,'bdx'),(59,'ben'),(60,'bhi'),(61,'biib'),(62,'bk'),(63,'blk'),(64,'bll'),(65,'bmy'),(66,'brk_b'),(67,'bsx'),(68,'bwa'),(69,'bxlt'),(70,'c'),(71,'ca'),(72,'cag'),(73,'cah'),(74,'cam'),(75,'car'),(76,'cat'),(77,'cb'),(78,'cbg'),(79,'cbs'),(80,'cce'),(81,'cci'),(82,'ccl'),(83,'celg'),(84,'cern'),(85,'cf'),(86,'cfg'),(87,'chd'),(88,'chk'),(89,'chrw'),(90,'ci'),(91,'cien'),(92,'cinf'),(93,'cl'),(94,'clf'),(95,'clx'),(96,'cma'),(97,'cmcsa'),(98,'cme'),(99,'cmg'),(100,'cmi'),(101,'cms'),(102,'cnc'),(103,'cnp'),(104,'cnx'),(105,'cof'),(106,'cog'),(107,'coh'),(108,'col'),(109,'cop'),(110,'cost'),(111,'cpb'),(112,'cpgx'),(113,'crm'),(114,'csc'),(115,'csco'),(116,'csx'),(117,'ctl'),(118,'ctsh'),(119,'ctxs'),(120,'cvc'),(121,'cvs'),(122,'cvx'),(123,'cxo'),(124,'d'),(125,'dal'),(126,'dd'),(127,'ddr'),(128,'de'),(129,'df'),(130,'dfs'),(131,'dg'),(132,'dgx'),(133,'dhi'),(134,'dhr'),(135,'dis'),(136,'disca'),(137,'dlph'),(138,'dltr'),(139,'dnr'),(140,'do'),(141,'dov'),(142,'dow'),(143,'dps'),(144,'dri'),(145,'dte'),(146,'duk'),(147,'dva'),(148,'dvn'),(149,'dyn'),(150,'ea'),(151,'ebay'),(152,'ecl'),(153,'ed'),(154,'eix'),(155,'el'),(156,'emc'),(157,'emn'),(158,'emr'),(159,'endp'),(160,'eog'),(161,'eqix'),(162,'eqr'),(163,'eqt'),(164,'es'),(165,'esrx'),(166,'esv'),(167,'etfc'),(168,'etn'),(169,'etr'),(170,'ew'),(171,'exc'),(172,'expd'),(173,'expe'),(174,'exr'),(175,'f'),(176,'fast'),(177,'fb'),(178,'fcx'),(179,'fdx'),(180,'fe'),(181,'ffiv'),(182,'fhn'),(183,'fis'),(184,'fisv'),(185,'fitb'),(186,'fl'),(187,'flr'),(188,'fls'),(189,'fmc'),(190,'fosl'),(191,'fox'),(192,'foxa'),(193,'fslr'),(194,'fti'),(195,'ftr'),(196,'gd'),(197,'ge'),(198,'ggp'),(199,'gild'),(200,'gis'),(201,'glw'),(202,'gm'),(203,'gme'),(204,'gnw'),(205,'goog'),(206,'googl'),(207,'gpc'),(208,'gps'),(209,'grmn'),(210,'gs'),(211,'gt'),(212,'gww'),(213,'hal'),(214,'har'),(215,'has'),(216,'hban'),(217,'hbi'),(218,'hca'),(219,'hcn'),(220,'hcp'),(221,'hd'),(222,'hes'),(223,'hig'),(224,'hlt'),(225,'hog'),(226,'hon'),(227,'hot'),(228,'hp'),(229,'hpe'),(230,'hpq'),(231,'hrb'),(232,'hrl'),(233,'hrs'),(234,'hsbc'),(235,'hst'),(236,'hsy'),(237,'ibm'),(238,'ice'),(239,'ilmn'),(240,'intc'),(241,'intu'),(242,'ip'),(243,'ipg'),(244,'ir'),(245,'irm'),(246,'itw'),(247,'ivz'),(248,'jbht'),(249,'jbl'),(250,'jci'),(251,'jcp'),(252,'jec'),(253,'jnj'),(254,'jnpr'),(255,'jns'),(256,'joy'),(257,'jpm'),(258,'jwn'),(259,'k'),(260,'kate'),(261,'kbh'),(262,'key'),(263,'kim'),(264,'klac'),(265,'kmb'),(266,'kmi'),(267,'kmx'),(268,'ko'),(269,'kors'),(270,'kr'),(271,'kss'),(272,'ksu'),(273,'l'),(274,'lb'),(275,'leg'),(276,'len'),(277,'lh'),(278,'lltc'),(279,'lly'),(280,'lm'),(281,'lmt'),(282,'lnc'),(283,'low'),(284,'lpx'),(285,'lrcx'),(286,'luk'),(287,'luv'),(288,'lvlt'),(289,'lyb'),(290,'m'),(291,'ma'),(292,'mac'),(293,'mar'),(294,'mas'),(295,'mat'),(296,'mbi'),(297,'mcd'),(298,'mchp'),(299,'mck'),(300,'mco'),(301,'mdlz'),(302,'mdr'),(303,'mdt'),(304,'met'),(305,'mhfi'),(306,'mjn'),(307,'mkc'),(308,'mmc'),(309,'mmm'),(310,'mnk'),(311,'mnst'),(312,'mo'),(313,'mon'),(314,'mos'),(315,'mpc'),(316,'mrk'),(317,'mro'),(318,'ms'),(319,'msft'),(320,'msi'),(321,'mtb'),(322,'mtg'),(323,'mtw'),(324,'mu'),(325,'mur'),(326,'mxim'),(327,'myl'),(328,'nav'),(329,'navi'),(330,'nbl'),(331,'nbr'),(332,'ncr'),(333,'ndaq'),(334,'ne'),(335,'nee'),(336,'nem'),(337,'nflx'),(338,'nfx'),(339,'ni'),(340,'nke'),(341,'nlsn'),(342,'noc'),(343,'nov'),(344,'nrg'),(345,'nsc'),(346,'ntap'),(347,'ntrs'),(348,'nue'),(349,'nvda'),(350,'nwl'),(351,'nwsa'),(352,'o'),(353,'odp'),(354,'oi'),(355,'oke'),(356,'omc'),(357,'orcl'),(358,'orly'),(359,'oxy'),(360,'payx'),(361,'pbct'),(362,'pbi'),(363,'pcar'),(364,'pcg'),(365,'pcln'),(366,'pdco'),(367,'peg'),(368,'pep'),(369,'pfe'),(370,'pfg'),(371,'pg'),(372,'pgr'),(373,'ph'),(374,'phm'),(375,'pld'),(376,'pm'),(377,'pnc'),(378,'pnr'),(379,'pnw'),(380,'ppg'),(381,'ppl'),(382,'prgo'),(383,'pru'),(384,'psa'),(385,'psx'),(386,'pvh'),(387,'pwr'),(388,'px'),(389,'pxd'),(390,'pypl'),(391,'qcom'),(392,'qep'),(393,'qrvo'),(394,'r'),(395,'rad'),(396,'rai'),(397,'rcl'),(398,'rdc'),(399,'regn'),(400,'rf'),(401,'rhi'),(402,'rht'),(403,'rig'),(404,'rl'),(405,'rlgy'),(406,'rok'),(407,'rost'),(408,'rrc'),(409,'rrd'),(410,'rsg'),(411,'rtn'),(412,'s'),(413,'sbux'),(414,'scg'),(415,'schw'),(416,'sci'),(417,'se'),(418,'see'),(419,'shw'),(420,'sig'),(421,'sjm'),(422,'slb'),(423,'slg'),(424,'slm'),(425,'sndk'),(426,'sni'),(427,'snv'),(428,'so'),(429,'spg'),(430,'spls'),(431,'srcl'),(432,'sre'),(433,'sti'),(434,'stj'),(435,'str'),(436,'stt'),(437,'stx'),(438,'stz'),(439,'sune'),(440,'svu'),(441,'swk'),(442,'swks'),(443,'swn'),(444,'syf'),(445,'syk'),(446,'symc'),(447,'syy'),(448,'t'),(449,'tap'),(450,'tdc'),(451,'te'),(452,'tel'),(453,'ter'),(454,'tgna'),(455,'tgt'),(456,'thc'),(457,'tif'),(458,'tjx'),(459,'tmk'),(460,'tmo'),(461,'tmus'),(462,'trip'),(463,'trow'),(464,'trv'),(465,'tsco'),(466,'tsn'),(467,'tso'),(468,'tss'),(469,'twc'),(470,'twx'),(471,'txn'),(472,'txt'),(473,'tyc'),(474,'ua'),(475,'ual'),(476,'un'),(477,'unh'),(478,'unm'),(479,'unp'),(480,'ups'),(481,'urbn'),(482,'uri'),(483,'usb'),(484,'utx'),(485,'v'),(486,'var'),(487,'vfc'),(488,'viab'),(489,'vlo'),(490,'vmc'),(491,'vno'),(492,'vrsk'),(493,'vrsn'),(494,'vrtx'),(495,'vtr'),(496,'vz'),(497,'wba'),(498,'wdc'),(499,'wec'),(500,'wfc'),(501,'wfm'),(502,'wft'),(503,'whr'),(504,'win'),(505,'wm'),(506,'wmb'),(507,'wmt'),(508,'wpx'),(509,'wu'),(510,'wy'),(511,'wyn'),(512,'wynn'),(513,'x'),(514,'xec'),(515,'xel'),(516,'xl'),(517,'xlnx'),(518,'xom'),(519,'xray'),(520,'xrx'),(521,'xyl'),(522,'yhoo'),(523,'yum'),(524,'zbh'),(525,'zion'),(526,'zts');
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade`
--

DROP TABLE IF EXISTS `trade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accounted_for` bit(1) NOT NULL,
  `last_state_change` datetime DEFAULT NULL,
  `price` double NOT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `trade_type` int(11) DEFAULT NULL,
  `strategy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8pmjp2f325a27rgn5rrw7ir0g` (`strategy_id`),
  CONSTRAINT `FK8pmjp2f325a27rgn5rrw7ir0g` FOREIGN KEY (`strategy_id`) REFERENCES `moving_average_strategy` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade`
--

LOCK TABLES `trade` WRITE;
/*!40000 ALTER TABLE `trade` DISABLE KEYS */;
/*!40000 ALTER TABLE `trade` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-03  9:31:32
