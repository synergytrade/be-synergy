package com.citi.training.trader.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.MovingAverageDao;
import com.citi.training.trader.model.MovingAverageStrategy;
import com.google.common.collect.Lists;


@Component
public class MovingAverageStrategyService {

	@Autowired
	private MovingAverageDao movingAverageDao;
	
	public List<MovingAverageStrategy> findAll(){
		return Lists.newArrayList(movingAverageDao.findAll());
	}
	
	public List<MovingAverageStrategy> findAllCurrent(){
		ArrayList<MovingAverageStrategy> stoppedStrats = new ArrayList();
		for(MovingAverageStrategy strategy: Lists.newArrayList(movingAverageDao.findAll()) ) {
			if(strategy.getStopped() == null) {
				stoppedStrats.add(strategy);
			}
		}
		return stoppedStrats;
	}
	
	public MovingAverageStrategy save(MovingAverageStrategy strat) {
		return movingAverageDao.save(strat);
	}
	
	public void deleteById(int id) {
		movingAverageDao.deleteById(id);
	}
	
	public MovingAverageStrategy getById(int id) {
		return movingAverageDao.findById(id).get();
	}
}
