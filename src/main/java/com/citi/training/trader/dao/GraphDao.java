package com.citi.training.trader.dao;

import java.util.List;


import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Graph;
import com.citi.training.trader.model.MovingAverageStrategy;
/**
 * 
 * Creates a DAO object for the graphs method, linking it to the db vis the crud repository
 *
 */
public interface GraphDao extends CrudRepository<Graph, Integer>{
	
	List<Graph> findByStrat(MovingAverageStrategy strat);

}