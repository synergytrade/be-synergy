package com.citi.training.trader.strategy;

import java.util.Date;
import java.util.List;
import java.lang.Math;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Graph;
import com.citi.training.trader.model.MovingAverageStrategy;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.service.GraphService;
import com.citi.training.trader.service.MovingAverageStrategyService;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.TradeService;

/**
 * Algorithm to calculate when to buy and sell using Moving average cross over.
 * 
 * <p> 
 * This is simplify taking two averages a long and short. When the cross over each other you act
 *  </p>
 *
 */
@Profile("!no-scheduled")
@Component
public class MovingAverageStrategyAlgorithm implements StrategyAlgorithm {
	
    private static final Logger logger =
            LoggerFactory.getLogger(MovingAverageStrategyAlgorithm.class);

	@Autowired
	private TradeSender tradeSender;
	
	@Autowired
	private PriceService priceService;
	
	@Autowired
	private MovingAverageStrategyService strategyService;
	
	@Autowired
	private TradeService tradeService;
	
	@Autowired
	private GraphService graphService;
	
	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	 public void run() {

    	// Loops trough all strategies
        for(MovingAverageStrategy strategy: strategyService.findAll()) {
        	
        	// makes sure strategies is there
            Trade lastTrade = null;
            try {
                lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
            } catch(EntityNotFoundException ex) {
                logger.debug("No Trades for strategy id: " + strategy.getId());
            }
            
            
            // if you have asked it to stop
            if(strategy.getStopped() != null) {
            	//TODO:  need to be able to say do you want to continue or sell! (Prob froint end)
            	if(lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
                    logger.debug("Waiting for reply, trying to close");
                    continue;
            	}else if(lastTrade.getState() == TradeState.REJECTED) {
            		tradeService.deleteById(lastTrade.getId());
            		continue;
            	}else if(!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) {
            		if(strategy.hasLongPosition()) {
                        logger.debug("Closing long position for strategy: " + strategy);
                        logger.debug("Bought at: " + strategy.getLastTradePrice() + ", sold at: " +
                                     lastTrade.getPrice());
                        double profit = lastTrade.getPrice() - strategy.getLastTradePrice();
                        strategy.setPrevProfit(profit);
                        closePosition(profit, strategy);
                    }
                    else if(strategy.hasShortPosition()) {
                        logger.debug("Closing short position for strategy: " + strategy);
                        logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " +
                                     lastTrade.getPrice());
                        double profit = strategy.getLastTradePrice() - lastTrade.getPrice();
                        strategy.setPrevProfit(profit);
                        closePosition(profit, strategy);
                    }
            		strategyService.save(strategy);
                    lastTrade.setAccountedFor(true);
                    tradeService.save(lastTrade);
            	}
            	if(strategy.hasPosition()) {
            		if(strategy.hasLongPosition()) {
            			makeTrade(strategy, Trade.TradeType.SELL);
            		}else {
            			makeTrade(strategy, Trade.TradeType.BUY);
            		}
            		strategyService.save(strategy);
            	}
                continue;
            }
            
            
            // If there is a prev trade
            if(lastTrade != null) {
                if(lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
                    logger.debug("Waiting for last trade to complete, do nothing");
                    continue;
                }           
                else if(!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) 
                {
                    logger.debug("Accounting for Trade: " + lastTrade);
                    
                    if(!strategy.hasPosition()) {         	
                        if(lastTrade.getTradeType() == Trade.TradeType.SELL) {
                            logger.debug("Confirmed short position for strategy: " + strategy);
                            strategy.takeShortPosition();
                        }
                        else {
                            logger.debug("Confirmed long position for strategy: " + strategy);
                            strategy.takeLongPosition();
                        }
                        strategy.setLastTradePrice(lastTrade.getPrice());
                    }
                    else {
                    	if(strategy.hasLongPosition()) {
                            logger.debug("Closing long position for strategy: " + strategy);
                            logger.debug("Bought at: " + strategy.getLastTradePrice() + ", sold at: " +
                                         lastTrade.getPrice());
                            double profit = lastTrade.getPrice() - strategy.getLastTradePrice();
                            strategy.setPrevProfit(profit);
                            closePosition(profit, strategy);
                        }
                        else if(strategy.hasShortPosition()) {
                            logger.debug("Closing short position for strategy: " + strategy);
                            logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " +
                                         lastTrade.getPrice());
                            double profit = strategy.getLastTradePrice() - lastTrade.getPrice();
                            strategy.setPrevProfit(profit);
                            closePosition(profit, strategy);
                        }
                    }
                    strategyService.save(strategy);
                    lastTrade.setAccountedFor(true);
                    tradeService.save(lastTrade);
                    
             }else if(!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.REJECTED)
            	 // Remove the last trade
                tradeService.deleteById(lastTrade.getId());
                
            }
        	// calculating averages
        	double shortAvg = this.cacluateShortAvg(strategy);
        	double longAvg = this.cacluateLongAvg(strategy);
        	// ********* store values, current date and time
        	if(shortAvg == 0 || longAvg == 0) {
        		continue;
        	}
        	
        	Graph graphData = new Graph();
        	graphData.setAvgShort(shortAvg);
        	graphData.setAvgLong(longAvg);
        	graphData.setStratId(strategy);
        	graphService.save(graphData);
        	
        	
        	// TODO: need to implement the precentage thing..
        	// double precentage = Math.abs(shortAvg/longAvg - 1);

        	// If you are in long posisiton
        	if(!strategy.hasPosition()) {
        		// TODO: above cannot be null.. thus may need to be -1, 1 and 0 to
        		// tell if its the first round or not
        		if(strategy.isStart()) {
        			if(shortAvg < longAvg) {
        				strategy.setAbove(false);
        			}else {
        				strategy.setAbove(true);
        			}
        			strategy.setStart(false);
        			strategyService.save(strategy);
        			continue;
        		}else {
        			if(shortAvg < longAvg && strategy.isAbove() ) {
            			logger.debug("I am Selling, I had no prev position: now short");
            			makeTrade(strategy, Trade.TradeType.SELL);
            			strategy.setAbove(false);
            		}else if(shortAvg > longAvg && !strategy.isAbove()) {
            			logger.debug("I am buying, I had no prev position: now : now long");
            			makeTrade(strategy, Trade.TradeType.BUY);
            			strategy.setAbove(true);
            		}
        		}
        	}
        	else if(strategy.hasShortPosition()) {
        		if( shortAvg > longAvg && !strategy.isAbove()) {
        			logger.debug("Have a Short postions: Attpemting to Buy");
        			makeTrade(strategy, Trade.TradeType.BUY);
        			strategy.setAbove(true);
        		}else {
        			if(shortAvg > longAvg) {
        				strategy.setAbove(true);
        			}else {
        				strategy.setAbove(false);
        			}
        			logger.debug("Have a short potions: holding");
        			continue;
        		}
        	}else {
        		if(shortAvg < longAvg && strategy.isAbove()) {
        			logger.debug("Have a long postions: Attpemting to Sell");
        			makeTrade(strategy, Trade.TradeType.SELL);
        			strategy.setAbove(false);
        		}else {
        			if(shortAvg > longAvg) {
        				strategy.setAbove(true);
        			}else {
        				strategy.setAbove(false);
        			}
        			logger.debug("Have a long postion: holding");
        			continue;
        		}
        	}
        	strategyService.save(strategy);
	    }
	}

	
	public double cacluateShortAvg(MovingAverageStrategy strategy) {
		// short period is in mins
		// TODO: need to put back *60 
		logger.debug("Calculation short Avg");
		int numberOfShortStocks = strategy.getShortPeriod();
		
        Double sumShort = 0.0;
        Double shortAvg = 0.0;
        List<Price> prices = priceService.findLatest(strategy.getStock(),numberOfShortStocks);
        if(prices.size() < numberOfShortStocks) {
            logger.warn("Unable to execute strategy, not enough price data: " +
                        strategy); 
            return 0;
        }
        
        for(int j = 0; j < numberOfShortStocks; j++){
            sumShort += prices.get(j).getPrice();
        }

        //avg for short:
        shortAvg = sumShort/numberOfShortStocks;

        //store average to db
        //INSERT()

        return shortAvg;
	}
	
	public double cacluateLongAvg(MovingAverageStrategy strategy) {
		logger.debug("Calculating long average");
		// short var is in mins 
		// TODO: Need to put back 60*60
		int numberOfLongStocks = strategy.getLongPeriod()*60;
		
        Double sumLong = 0.0;
        Double longAvg = 0.0;
        List<Price> prices = priceService.findLatest(strategy.getStock(),numberOfLongStocks);
        if(prices.size() < numberOfLongStocks) {
            logger.warn("Unable to execute strategy, not enough price data: " +
                        strategy); 
            return 0;
        }
        
        for(int j = 0; j < numberOfLongStocks; j++){
            sumLong += prices.get(j).getPrice();
        }

        //avg for short:
        longAvg = sumLong/numberOfLongStocks;

        //store average to db
        //INSERT()

        return longAvg;
	}
	
	
	
	

    private void closePosition(double profitLossPerShare, MovingAverageStrategy strategy) {
        double totalProfitLoss = profitLossPerShare * strategy.getSize();

        logger.debug("Recording profit/loss of: " + totalProfitLoss +
                     " for strategy: " + strategy);
        strategy.addProfitLoss(totalProfitLoss);
        strategy.closePosition();

        // We can discuss if we want this or not
        if(Math.abs(strategy.getProfit()) >= strategy.getExitProfitLoss()) {
            logger.debug("Exit condition reached, exiting strategy");
            strategy.setStopped(new Date());
            strategyService.save(strategy);
        }
    }


    private double makeTrade(MovingAverageStrategy strategy, Trade.TradeType tradeType) {
        Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
        tradeSender.sendTrade(new Trade(currentPrice.getPrice(),
                                        strategy.getSize(), tradeType,
                                        strategy));
        return currentPrice.getPrice();
    }

}
