package com.citi.training.trader.pricefeed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

/**
 * Price feed that is getting data from the API and puting it in a price object.
 *
 */

@Component
public class ConygreFeed2 implements PriceFeed {
    
    private static final Logger LOG = LoggerFactory.getLogger(ConygreFeed2.class);

    @Value("${com.citi.training.trader.pricefeed.feed2.url:http://feed2.conygre.com}")
    private String priceFeedUrl;

    public List<Price> findLatest(Stock stock, int count) {
        RestTemplate restTemplate = new RestTemplate();

        String url  = priceFeedUrl + "/API/StockFeed/GetStockPricesForSymbol/" +
                      stock.getTicker() + "?HowManyValues=" + count;


        ResponseEntity<List<ConygreFeed2Dto>> feedResponse =
                restTemplate.exchange(url,
                                      HttpMethod.GET,
                                      null,
                            new ParameterizedTypeReference<List<ConygreFeed2Dto>>() {});


        List<Price> foundPrices = new ArrayList<Price>();

        for(ConygreFeed2Dto feedRecord: feedResponse.getBody()) {
            foundPrices.add(feedRecord.toPrice());
        }

        Collections.reverse(foundPrices);
        return foundPrices;
    }
}
