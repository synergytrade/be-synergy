package com.citi.training.trader.pricefeed;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConygreFeed2Dto {

    private static final String  DATE_FORMAT = "hh:mm:ss";
    private static final Logger LOG = LoggerFactory.getLogger(ConygreFeed2Dto.class);

    @JsonProperty("Symbol")
    private String symbol;

    @JsonProperty("Price")
    private double price;

    private String tradeTime;

    @JsonProperty("CompanyName")
    private String companyName;

    private int periodNumber;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getPeriodNumber() {
        return periodNumber;
    }

    public void setPeriodNumber(int periodNumber) {
        this.periodNumber = periodNumber;
    }

    public Price toPrice() {
        try {
            Date recordedAt = new SimpleDateFormat(DATE_FORMAT).parse(this.tradeTime);

            return new Price(new Stock(-1, this.symbol),
                             this.price,
                             recordedAt);
        } catch(ParseException ex) {
            String msg = "Failed to parse Date format [" + DATE_FORMAT +
                         "] from price feed trade Time: [" + this.tradeTime + "]";

            LOG.error(msg);
            throw new EntityNotFoundException(msg);
        }
    }

    @Override
    public String toString() {
        return "ConygreFeed2Dto [Symbol=" + symbol + ", Price=" + price +
               ", tradeTime=" + tradeTime + ", CompanyName=" + companyName +
               ", periodNumber=" + periodNumber + "]";
    }
}
