package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.pricefeed.PriceFeed;

@Component
public class PriceService {

    @Autowired
    private PriceFeed priceFeed;

    public List<Price> findLatest(Stock stock, int count) {
        return priceFeed.findLatest(stock,  count);
    }
}
