package com.citi.training.trader.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Stock;


/**
 * 
 * Creates a DAO object for the Stock method, linking it to the db vis the crud repository
 *
 */
public interface StockDao extends CrudRepository<Stock, Integer> {

    Stock findByTicker(String ticker);

}
